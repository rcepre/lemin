/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   stroke_calcul.c                                  .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rcepre <rcepre@student.le-101.fr>          +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/03/01 13:47:16 by krambono     #+#   ##    ##    #+#       */
/*   Updated: 2019/06/24 04:55:44 by rcepre      ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "lem_in.h"

static void		norme(t_vars *vars, t_dwaylist **list)
{
	clear_dwaylist_half(list);
	error_alloc(vars, NULL, NULL);
}

int				get_strokes_min(t_dwaylist *list, int size, int nb_ant)
{
	int y;

	y = nb_ant;
	while (list)
	{
		y += list->dway->len;
		list = list->next;
	}
	y /= size;
	return (y);
}

static void		strokes_calcul_2(int *strokes_min, int strokes,
				int *count_min, int count)
{
	if (strokes < *strokes_min)
	{
		*count_min = count;
		*strokes_min = strokes;
	}
}

int				strokes_calcul(__uint128_t nb, t_vars *vars)
{
	int			i;
	int			count;
	int			count_min;
	int			strokes_min;
	t_dwaylist	*list;

	i = 0;
	list = NULL;
	count = 0;
	count_min = 0;
	strokes_min = FT_INT_MAX;
	while (i < vars->size)
	{
		if (!!(nb & ((__uint128_t)1 << (vars->size - 1 - i))) == 1)
		{
			if (!(dwaylist_add_elem_back(&list, vars->tab_baseways[i])))
				norme(vars, &list);
			count++;
			strokes_calcul_2(&strokes_min, get_strokes_min(list, count,
			vars->antfarm->nb_ant), &count_min, count);
		}
		i++;
	}
	clear_dwaylist_half(&list);
	return ((nb >> 127) != 1 ? strokes_min : count_min);
}
