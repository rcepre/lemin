/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   t_queue.c                                        .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rcepre <rcepre@student.le-101.fr>          +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/02/12 14:48:24 by krambono     #+#   ##    ##    #+#       */
/*   Updated: 2019/06/24 04:55:40 by rcepre      ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "lem_in.h"

t_queue		*create_elem_queue(int data)
{
	t_queue *queue;

	if (!(queue = (t_queue*)ft_memalloc(sizeof(t_queue))))
		return (NULL);
	queue->data = data;
	queue->next = NULL;
	return (queue);
}

int			enqueue(t_queue **queue, int data)
{
	t_queue *tmp;

	if (!*queue)
	{
		if (!(*queue = create_elem_queue(data)))
			return (0);
	}
	else
	{
		tmp = *queue;
		while (tmp->next)
			tmp = tmp->next;
		if (!(tmp->next = create_elem_queue(data)))
			return (0);
	}
	return (1);
}

int			dequeue(t_queue **queue)
{
	int		tmp;
	t_queue *tmp_queue;

	if (!*queue)
		return (-1);
	tmp = (*queue)->data;
	tmp_queue = *queue;
	*queue = (*queue)->next;
	free(tmp_queue);
	return (tmp);
}
