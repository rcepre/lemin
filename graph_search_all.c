/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   graph_search_all.c                               .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rcepre <rcepre@student.le-101.fr>          +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/06/24 04:55:17 by rcepre       #+#   ##    ##    #+#       */
/*   Updated: 2019/06/24 04:55:19 by rcepre      ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */


#include "lem_in.h"

static void			clean(t_dway **dway, t_dwaylist **list, t_queue **queue)
{
	while (*queue)
		dequeue(queue);
	error_alloc(NULL, dway, list);
}

static void			norme(t_queue **queue, t_dwaylist **list, int room_i)
{
	if (!enqueue(queue, room_i))
	{
		while (*queue)
			dequeue(queue);
		error_alloc(NULL, NULL, list);
	}
}

static int			graph_search_all_2(int room_i, int end, t_dwaylist **list,
																t_queue **queue)
{
	t_dway		*tmp;
	t_dway		*dway;
	t_dwaylist	*tmp_list;

	tmp_list = find_dway_all(*list, end);
	tmp = tmp_list ? tmp_list->dway : NULL;
	if (!search_room(tmp, room_i))
	{
		dway = dway_copy(tmp);
		if (tmp && !dway)
		{
			while (*queue)
				dequeue(queue);
			error_alloc(NULL, NULL, list);
		}
		if (!dway)
			if (!dway_add_elem_front(&dway, end))
				clean(&dway, list, queue);
		if (!dway_add_elem_front(&dway, room_i))
			clean(&dway, list, queue);
		if (!dwaylist_add_elem_back(list, dway))
			clean(&dway, list, queue);
		return (1);
	}
	return (0);
}

static t_queue		*graph_search_all_1(t_antfarm *antfarm, int end, int count,
															t_dwaylist **list)
{
	t_queue		*queue;
	t_linklist	*links;
	t_dwaylist	*tmp_list;

	queue = NULL;
	if (!enqueue(&queue, end))
		error_alloc(NULL, NULL, list);
	while (queue)
	{
		end = dequeue(&queue);
		links = antfarm->rooms[end]->linklist;
		tmp_list = find_dway_all(*list, end);
		while (links && end != antfarm->start)
		{
			if (graph_search_all_2(links->room_i, end, list, &queue))
				norme(&queue, list, links->room_i);
			links->room_i == antfarm->start && ++count;
			links = links->next;
		}
		if (tmp_list && tmp_list->dway->head->room_i != antfarm->start)
			dwaylist_remove_elem(list, tmp_list);
		if (count == 100)
			break ;
	}
	return (queue);
}

void				graph_search_all(t_antfarm *antfarm, t_dwaylist **list)
{
	t_queue *queue;

	queue = graph_search_all_1(antfarm, antfarm->end, 0, list);
	while (queue)
		dequeue(&queue);
	clear_dwaylist_if(list, antfarm->start);
	dwaylist_remove_dups(list);
}
