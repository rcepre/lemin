/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   t_dwaylist.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: krambono <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/02/11 14:18:20 by krambono     #+#   ##    ##    #+#       */
/*   Updated: 2019/03/12 17:36:01 by krambono    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "lem_in.h"

t_dwaylist		*create_dwaylist(t_dway *dway)
{
	t_dwaylist	*list;

	if (!(list = (t_dwaylist*)ft_memalloc(sizeof(t_dwaylist))))
		return (NULL);
	list->dway = dway;
	list->next = NULL;
	return (list);
}

int				dwaylist_add_elem_back(t_dwaylist **begin, t_dway *dway)
{
	t_dwaylist *tmp;

	if (!*begin)
	{
		if (!(*begin = create_dwaylist(dway)))
			return (0);
	}
	else
	{
		tmp = (*begin);
		while (tmp->next)
			tmp = tmp->next;
		if (!(tmp->next = create_dwaylist(dway)))
			return (0);
	}
	return (1);
}

int				dwaylist_add_elem_front(t_dwaylist **begin, t_dway *dway)
{
	t_dwaylist *tmp;

	if (!(tmp = create_dwaylist(dway)))
		return (0);
	tmp->next = *begin;
	*begin = tmp;
	return (1);
}

void			dwaylist_remove_elem(t_dwaylist **list, t_dwaylist *ref)
{
	t_dwaylist *tmp;
	t_dwaylist *tmp2;

	if (!*list || !ref)
		return ;
	if (*list == ref)
	{
		tmp = (*list)->next;
		clear_dway(&((*list)->dway));
		free(*list);
		*list = tmp;
		return ;
	}
	tmp = *list;
	tmp2 = tmp->next;
	while (tmp2 && tmp2 != ref)
	{
		tmp = tmp->next;
		tmp2 = tmp->next;
	}
	tmp->next = tmp2->next;
	clear_dway(&(tmp2->dway));
	free(tmp2);
}

void			clear_dwaylist(t_dwaylist **begin)
{
	if (!*begin)
		return ;
	if ((*begin)->next)
		clear_dwaylist(&((*begin)->next));
	clear_dway(&((*begin)->dway));
	free(*begin);
	*begin = NULL;
}
