/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   parser.c                                         .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rcepre <rcepre@student.le-101.fr>          +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/01/07 11:28:35 by krambono     #+#   ##    ##    #+#       */
/*   Updated: 2019/06/24 04:55:46 by rcepre      ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "lem_in.h"

void	get_file_data2(char **line, t_antfarm *antfarm, int *k, int *count_room)
{
	static t_roomlist	*roomlist;

	if (*line[0] == '#' && *k != ERROR_FILE && *k != END_FILE)
	{
		if (!check_hashtag(*line, antfarm, *count_room))
			*k = ERROR_FILE;
	}
	else
	{
		if (*k == NB_ANT)
			*k = check_nb_ant(*line, &(antfarm->nb_ant));
		else if (*k == ROOMS)
		{
			(*count_room)++;
			*k = check_rooms(*line, &roomlist);
			if (*k == LINKS)
				list_to_tab_rooms(&roomlist, antfarm);
		}
		if (*k == LINKS)
			*k = check_links(*line, antfarm);
	}
}

int		get_file_data(char **line, t_antfarm *antfarm, int fd)
{
	int			ret;
	int			k;
	int			count_room;

	count_room = 0;
	k = NB_ANT;
	while ((ret = get_next_line(fd, line, GNL)) == 1)
	{
		get_file_data2(line, antfarm, &k, &count_room);
		if (k == LINKS && antfarm->start == count_room - 1)
			k = ERROR_FILE;
		if (k != ERROR_FILE && k != END_FILE)
			ft_putbuf(*line, ADD);
		else
			free(*line);
	}
	if (ret == -1 || k == ERROR_FILE)
		return (ERROR_FILE);
	else
		return (END_FILE);
}

int		parser(t_antfarm *antfarm, char *file)
{
	int			fd;
	int			k;
	char		*line;

	if ((fd = file == NULL ? 0 : open(file, O_RDONLY)) == -1)
		return (0);
	k = get_file_data(&line, antfarm, fd);
	if (fd > 0)
		close(fd);
	if (k == ERROR_FILE || antfarm->nb_ant == -1 || antfarm->rooms == NULL ||
			antfarm->nb_room == 0 || antfarm->start == -1 || antfarm->end == -1
											|| (antfarm->start == antfarm->end))
	{
		ft_putbuf(NULL, FREE);
		my_heap(NULL, FREE);
		get_next_line(fd, &line, FREE);
		return (0);
	}
	return (1);
}
