/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   lem_in.c                                         .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: krambono <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/01/02 09:49:05 by krambono     #+#   ##    ##    #+#       */
/*   Updated: 2019/03/15 14:00:58 by krambono    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "lem_in.h"

int			main(int ac, char **av)
{
	t_antfarm	antfarm;
	int			ret;
	int			ops;
	int			tmp;

	if ((ret = ft_options_encode(av, ac, "mod")) < 0)
		ft_option_error(ret, "lem-in", "mod");
	tmp = ac - ret;
	if (tmp > 2)
	{
		ft_putendl_fd("usage : lem-in -mod [map]", 2);
		return (1);
	}
	init_antfarm(&antfarm);
	my_heap(&antfarm, ADD);
	if (!parser(&antfarm, tmp == 1 ? 0 : av[ac - 1]))
	{
		ft_putendl_fd("ERROR", 2);
		return (1);
	}
	if ((ops = resolve(&antfarm)) && ft_options("d", DECODE))
		display_infos(antfarm, ops, tmp == 1 ? "Standard input" : av[ac - 1]);
	clear_antfarm(&antfarm);
	return (0);
}
