/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   transform_room.c                                 .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: krambono <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/01/07 15:50:14 by krambono     #+#   ##    ##    #+#       */
/*   Updated: 2019/03/15 14:11:50 by krambono    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "lem_in.h"

t_room	*line_to_room(char *line)
{
	t_room	*room;
	int		len;

	if (!(room = (t_room*)ft_memalloc(sizeof(t_room))))
		error_alloc(NULL, NULL, NULL);
	len = ft_search_chr(line, ' ');
	room->mark = 0;
	room->ant = 0;
	room->name = ft_strsub(line, 0, len);
	room->x = ft_atoi(line + len + 1);
	room->y = ft_atoi(line + len + 1 + ft_search_chr(line + len + 1, ' '));
	room->linklist = NULL;
	if (room->name == NULL)
		error_alloc(NULL, NULL, NULL);
	return (room);
}

void	list_to_tab_rooms(t_roomlist **roomlist, t_antfarm *antfarm)
{
	int			count;
	int			i;
	t_roomlist	*tmp;

	count = count_roomlist(*roomlist);
	if (!(antfarm->rooms = (t_room**)ft_memalloc(sizeof(t_room*) * count)))
		error_alloc(NULL, NULL, NULL);
	i = 0;
	tmp = *roomlist;
	while (i < count)
	{
		antfarm->rooms[i] = tmp->room;
		tmp = tmp->next;
		i++;
	}
	antfarm->nb_room = count;
	my_heap(NULL, ROOMLIST);
	*roomlist = NULL;
}
