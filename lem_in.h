/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   lem_in.h                                         .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: krambono <krambono@student.42.fr>          +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/01/02 09:49:10 by krambono     #+#   ##    ##    #+#       */
/*   Updated: 2019/03/18 12:39:47 by rcepre      ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#ifndef LEM_IN_H
# define LEM_IN_H

# include "libft/includes/libft.h"
# include <fcntl.h>

# define NO_LIMIT			2147483647
# define ERROR_FILE			10
# define END_FILE			5
# define NB_ANT				0
# define ROOMS				1
# define LINKS				2
# define DISPLAY			1
# define GNL				1
# define FREE				2
# define ADD				3
# define ANTFARM			4
# define ROOMLIST			5

typedef struct				s_linklist
{
	int						room_i;
	int						mark;
	struct s_linklist		*next;
}							t_linklist;

typedef struct				s_room
{
	char					*name;
	int						x;
	int						y;
	int						mark;
	int						ant;
	t_linklist				*linklist;
}							t_room;

typedef struct				s_roomlist
{
	t_room					*room;
	struct s_roomlist		*next;
}							t_roomlist;

typedef struct				s_antfarm
{
	t_room					**rooms;
	int						nb_ant;
	int						nb_room;
	int						nb_links;
	int						start;
	int						end;
}							t_antfarm;

typedef struct				s_lines_file
{
	char					*line;
	struct s_lines_file		*next;
}							t_lines_file;

typedef struct				s_way
{
	int						room_i;
	int						pnodes;
	struct s_way			*next;
}							t_way;

typedef struct				s_dway
{
	unsigned int			len;
	int						ants;
	t_way					*head;
}							t_dway;

typedef struct				s_dwaylist
{
	t_dway					*dway;
	struct s_dwaylist		*next;
}							t_dwaylist;

typedef struct				s_queue
{
	int						data;
	struct s_queue			*next;
}							t_queue;

typedef struct				s_antlist
{
	t_dway					*dway;
	int						ant_id;
	struct s_antlist		*next;
}							t_antlist;

typedef struct				s_vars
{
	__uint128_t				*comb_tab;
	int						size;
	t_dway					**tab_baseways;
	t_antfarm				*antfarm;
	t_dwaylist				**baseways;
}							t_vars;

int							resolve(t_antfarm *antfarm);
void						init_antfarm(t_antfarm *antfarm);

/*
** GRAPH SEARCH
*/
void						graph_search(t_antfarm *antfarm, int start, int end,
															t_dwaylist **list);
void						graph_search_all(t_antfarm *antfarm,
															t_dwaylist **list);
t_dwaylist					*find_dway_all(t_dwaylist *list, int data);
int							search_room(t_dway *dway, int room_i);

/*
** PARSER
*/
int							check_hashtag(char *line, t_antfarm *antfarm,
																int count_room);
int							check_nb_ant(char *line, int *nb_ant);
int							check_rooms(char *line, t_roomlist **roomlist);
int							check_links(char *line, t_antfarm *antfarm);
int							parser(t_antfarm *antfarm, char *file);

/*
** WAY
*/
t_way						*create_way(int room_i);
void						clear_way(t_way **way);

/*
** DWAY
*/
t_dway						*create_dway(void);
void						clear_dway(t_dway **dway);
t_dway						*dway_copy(t_dway *dway);
int							dway_add_elem_back(t_dway **dway, int room_i);
int							dway_add_elem_front(t_dway **dway, int room_i);
void						dway_dequeue(t_dway **dway);
void						dway_reverse(t_dway **dway);
int							dways_are_differents(t_dway *d1, t_dway *d2);
int							dways_have_commons(t_dway *d1, t_dway *d2,
															t_antfarm *antfarm);

/*
** DWAY LIST
*/
void						clear_dwaylist_half(t_dwaylist **list);
t_dwaylist					*create_dwaylist(t_dway *dway);
void						clear_dwaylist(t_dwaylist **begin);
void						clear_dwaylist_if(t_dwaylist **begin,
																int reference);
int							dwaylist_add_elem_back(t_dwaylist **begin,
																t_dway *dway);
int							dwaylist_add_elem_front(t_dwaylist **begin,
																t_dway *dway);
void						dwaylist_reverse_all(t_dwaylist *list);
void						dwaylist_remove_elem(t_dwaylist **list,
															t_dwaylist *ref);
void						dwaylist_remove_dups(t_dwaylist **list);
t_dwaylist					*dwaylist_copy(t_dwaylist *list);
int							dwaylist_len(t_dwaylist *list);
int							dwaylist_len(t_dwaylist *list);
void						dwaylist_sort(t_dwaylist **list);
void						remove_from(t_dwaylist **list, int limit);

/*
** QUEUE
*/
t_queue						*create_elem_queue(int data);
int							enqueue(t_queue **queue, int data);
int							dequeue(t_queue **queue);

/*
** LINKLIST
*/
t_linklist					*create_linklist(int room_i);
void						linklist_add_elem(t_linklist **links, int room_i);
void						clear_linklist(t_linklist **links);

/*
** ROOMLIST
*/
t_roomlist					*create_roomlist(t_room *new_room);
void						add_room(t_room *new_room, t_roomlist **roomlist);
void						clear_room(t_room *room);
void						clear_roomlist(t_roomlist **roomlist, int choice);
int							count_roomlist(t_roomlist *roomlist);

/*
** TRANSFORM ROOM
*/
t_room						*line_to_room(char *line);
void						list_to_tab_rooms(t_roomlist **roomlist,
															t_antfarm *antfarm);
int							add_links_to_rooms(char *line, t_antfarm *antfarm);

/*
** LINES FILE
*/
t_lines_file				*create_lines_file(char *line);
void						clear_lines_file(t_lines_file **begin);
void						lines_file_add_elem(t_lines_file **begin,
																	char *line);

/*
** ANT LIST
*/
t_antlist					*create_antlist(t_dway *dway, int ant_nb);
void						clear_antlist(t_antlist **begin);
int							antlist_size(t_antlist *antlist);
void						antlist_add_elem(t_antlist **begin,
															t_antlist *elem);
void						antlist_remove_elem(t_antlist **list,
															t_antlist *ref);

/*
** UTILITAIRES
*/
int							check_name_exist(char *name, t_antfarm *antfarm);
void						clear_antfarm(t_antfarm *antfarm);
void						ft_putbuf(char *line, int choice);
void						my_heap(void *adr, int choice);
void						error_alloc(t_vars *vars, t_dway **dway,
														t_dwaylist **dwaylist);
int							ft_isalnum_custom(int c);
/*
** DISPLAY
*/
void						display_ant_moves(int new_line, int ant_id,
													char *room_name, int ops);
void						display_infos(t_antfarm antfarm, int ops,
																	char *name);

/*
** DISPLAY DEBUG
*/
void						matrix_display(t_antfarm antfarm,
															t_antlist *antlist);
void						display_antlist(t_antlist *begin);
void						display_dway(t_dway *dway);
void						display_antfarm(t_antfarm *antfarm);
void						display_dwaylist(t_dwaylist *begin);
void						display_lines_file(t_lines_file *begin);
void						display_dway_name(t_antfarm antfarm, t_dway *dway);
void						binary_display(__uint128_t nb, int size);
void						display_comb_tab(__uint128_t *comb_tab, int size);

/*
** WAYS FILTER
*/
void						ways_filter(t_dwaylist **baseways,
															t_antfarm *antfarm);

/*
** GET COMBS
*/
t_dwaylist					*get_combs(t_vars *vars);
t_dwaylist					*get_comb_list(__uint128_t comb, t_vars *vars);
void						get_real_way(__uint128_t nb, t_vars *vars);
__uint128_t					best_way(__uint128_t nb, t_vars *vars, int ret);
int							get_strokes_min(t_dwaylist *list, int size,
																	int nb_ant);
int							strokes_calcul(__uint128_t nb, t_vars *vars);
#endif
