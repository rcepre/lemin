/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   t_roomlist.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rcepre <rcepre@student.le-101.fr>          +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/01/07 15:46:14 by krambono     #+#   ##    ##    #+#       */
/*   Updated: 2019/06/24 04:55:39 by rcepre      ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "lem_in.h"

t_roomlist	*create_roomlist(t_room *new_room)
{
	t_roomlist	*roomlist;

	if (!(roomlist = (t_roomlist*)ft_memalloc(sizeof(t_roomlist))))
		error_alloc(NULL, NULL, NULL);
	roomlist->room = new_room;
	roomlist->next = NULL;
	return (roomlist);
}

void		add_room(t_room *new_room, t_roomlist **roomlist)
{
	t_roomlist *tmp;

	if (!*roomlist)
	{
		*roomlist = create_roomlist(new_room);
		my_heap(*roomlist, ADD);
	}
	else
	{
		tmp = *roomlist;
		while (tmp->next)
			tmp = tmp->next;
		tmp->next = create_roomlist(new_room);
	}
}

void		clear_room(t_room *room)
{
	if (room->name)
		ft_strdel(&(room->name));
	clear_linklist(&(room->linklist));
	free(room);
}

void		clear_roomlist(t_roomlist **roomlist, int choice)
{
	if (!*roomlist)
		return ;
	clear_roomlist(&((*roomlist)->next), choice);
	if (choice)
		clear_room((*roomlist)->room);
	free(*roomlist);
	*roomlist = NULL;
	roomlist = NULL;
}

int			count_roomlist(t_roomlist *roomlist)
{
	int count;

	count = 0;
	while (roomlist)
	{
		count++;
		roomlist = roomlist->next;
	}
	return (count);
}
