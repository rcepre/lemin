/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   parsing_utils.c                                  .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rcepre <rcepre@student.42.fr>              +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/11/22 07:18:30 by rcepre       #+#   ##    ##    #+#       */
/*   Updated: 2018/12/12 11:32:36 by rcepre      ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libftprintf.h"

int		get_attibutes(char *str, t_arg *arg, int len)
{
	int	i;

	i = 0;
	while (i < len)
	{
		if (str[i] == '#')
			arg->hash = 1;
		if (str[i] == '0')
			arg->zero_fill = 1;
		if (str[i] == ' ')
			arg->space = 1;
		if (str[i] == '+')
			arg->pos_sign = 1;
		if (str[i] == '-')
			arg->left = 1;
		i++;
	}
	return (arg->left + arg->hash + arg->zero_fill + arg->pos_sign);
}

int		get_modifier(t_arg *arg, char *str)
{
	int		i;
	int		j;
	char	*mods[6];

	mods[0] = "ll";
	mods[1] = "l";
	mods[2] = "hh";
	mods[3] = "h";
	mods[4] = "L";
	mods[5] = NULL;
	j = -1;
	while (str[++j] && !arg->mod)
	{
		i = -1;
		while (ft_isalpha(str[j]) && mods[++i] && !arg->mod)
			if (ft_strnstr(str + j, mods[i], 2))
			{
				arg->mod = mods[i];
				return (1);
			}
	}
	return (0);
}

int		get_type(char *str, t_arg *arg)
{
	int		i;
	int		j;
	char	types[15];

	ft_strcpy(types, "sc%diouxXfbp\0");
	j = 0;
	while (str[++j] && arg->type == 0)
	{
		i = -1;
		if (!str[j])
			return (-1);
		while (types[++i])
			if (str[j] == types[i])
				arg->type = types[i];
	}
	return (j);
}

int		get_field(char *str, t_arg *arg)
{
	int i;

	i = 0;
	arg->field = 0;
	while (str[i] && (str[i] == '0' || !ft_isdigit(str[i])))
		i++;
	while (str[i] && ft_isdigit(str[i]))
	{
		arg->field = arg->field * 10 + str[i] - '0';
		str[i] = '.';
		i++;
	}
	return (arg->field ? 1 : 0);
}

int		get_pre(char *str, t_arg *arg)
{
	int i;

	i = 0;
	arg->pre = -1;
	while (str[i])
	{
		if (str[i++] == '.')
		{
			arg->pre = 0;
			while (ft_isdigit(str[i]))
			{
				arg->pre = arg->pre * 10 + str[i] - '0';
				str[i] = '.';
				i++;
			}
		}
	}
	return (arg->pre >= 0 ? 1 : 0);
}
