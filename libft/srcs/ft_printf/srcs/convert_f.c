/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   convert_f.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rcepre <rcepre@student.42.fr>              +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/11/13 17:50:59 by rcepre       #+#   ##    ##    #+#       */
/*   Updated: 2018/12/17 13:52:13 by rcepre      ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libftprintf.h"

static int				get_field_size(long double nb, t_arg *arg)
{
	int size;

	size = flt_size(nb, arg);
	if (arg->pre || arg->hash)
		size += arg->pre + 1;
	if (arg->field > size)
		size = arg->field;
	return (size);
}

static void				int_part_to_str(long double *nb, char **field, \
																	t_arg *arg)
{
	long double	tmp;
	int			len;
	int			delta;

	if (*nb < 1)
		*(*field)++ = '0';
	tmp = *nb;
	while (!(*nb < 1))
	{
		len = 0;
		tmp = *nb;
		while (!(tmp < 10) && ++len)
			tmp /= 10;
		tmp = (int)tmp;
		*(*field)++ = (int)tmp + '0';
		while (len--)
			tmp *= 10;
		*nb = *nb - tmp;
		if ((delta = flt_size(tmp, arg) - flt_size(*nb, arg) - 1) > 0)
			while (delta--)
				*(*field)++ = '0';
	}
	if ((flt_size(tmp, arg) - flt_size(*nb, arg)) > 0)
		*(*field)++ = '0';
}

static void				dec_part_to_str(long double *nb, char **field, int pre,\
																	t_arg *arg)
{
	if (arg->pre || arg->hash)
	{
		*(*field)++ = '.';
		while (pre--)
		{
			*nb *= 10;
			*(*field)++ = (int)*nb + '0';
			*nb -= (int)*nb;
		}
	}
}

int						convert_f(long double nb, t_arg *arg)
{
	char	*field;
	char	*start;
	int		f_size;
	int		ret;

	if (!nb)
		nb = 0.0;
	if ((ret = filter(&nb, arg)) == -1)
		return (-1);
	if (ret == 1)
		return (ft_putstr(arg->data));
	f_size = get_field_size(nb, arg);
	if (!(field = ft_strnew(f_size)))
		return (-1);
	ft_memset(field, arg->zero_fill ? '0' : ' ', f_size);
	start = field;
	nb += get_rounder(arg->pre, nb);
	f_size -= (flt_size(nb, arg) + arg->pre + (arg->pre ? 1 : 0) + arg->hash);
	if (!arg->left)
		field += f_size;
	put_signs(arg, &field);
	int_part_to_str(&nb, &field, arg);
	dec_part_to_str(&nb, &field, arg->pre, arg);
	arg->data = start;
	return (ft_putstr(arg->data));
}
