/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   convert_f_utils.c                                .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rcepre <rcepre@student.42.fr>              +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/11/13 17:50:59 by rcepre       #+#   ##    ##    #+#       */
/*   Updated: 2018/12/11 13:21:34 by rcepre      ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libftprintf.h"

int				flt_size(long double n, t_arg *arg)
{
	int	count;

	if (n < 0)
		n = -n;
	count = 0;
	while (n >= 10)
	{
		n /= 10;
		count++;
	}
	count++;
	return (count + arg->neg_sign + arg->pos_sign + arg->space);
}

long double		get_rounder(int pre, long double nb)
{
	long double		rounder;
	unsigned long	tmp;

	tmp = (unsigned long)nb;
	rounder = 0.49;
	if (tmp % 2 || pre > 0)
		rounder = 0.5;
	while (pre-- > 0)
		rounder /= 10;
	return (rounder);
}

void			put_signs(t_arg *arg, char **field)
{
	if (arg->neg_sign)
		*(*field)++ = '-';
	else if (arg->space)
		*(*field)++ = ' ';
	else if (arg->pos_sign)
		*(*field)++ = '+';
}
