/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   utilities.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rcepre <rcepre@student.42.fr>              +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/11/22 06:55:27 by rcepre       #+#   ##    ##    #+#       */
/*   Updated: 2018/12/11 16:18:39 by rcepre      ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libftprintf.h"

t_arg		*init_t_arg(void)
{
	t_arg *elem;

	if (!(elem = (t_arg*)ft_memalloc(sizeof(t_arg))))
		return (NULL);
	elem->data = NULL;
	elem->mod = NULL;
	elem->type = 0;
	elem->hash = 0;
	elem->zero_fill = 0;
	elem->left = 0;
	elem->pos_sign = 0;
	elem->space = 0;
	elem->field = 0;
	elem->pre = -1;
	elem->neg_sign = 0;
	return (elem);
}
