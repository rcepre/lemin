/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   convert_percent.c                                .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rcepre <rcepre@student.42.fr>              +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/11/27 07:35:39 by rcepre       #+#   ##    ##    #+#       */
/*   Updated: 2018/12/02 20:27:16 by rcepre      ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libftprintf.h"

int		convert_percent(t_arg *arg)
{
	int		field_size;

	field_size = arg->field > 1 ? arg->field : 1;
	if (!(arg->data = ft_strnew(field_size)))
		return (-1);
	ft_memset(arg->data, arg->zero_fill ? '0' : ' ', field_size);
	if (arg->left)
		arg->data[0] = '%';
	else
		arg->data[field_size - 1] = '%';
	ft_putstr(arg->data);
	return (field_size);
}
