/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   libftprintf.h                                    .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rcepre <marvin@le-101.fr>                  +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/11/16 12:39:53 by rcepre       #+#   ##    ##    #+#       */
/*   Updated: 2019/03/15 13:58:09 by krambono    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#ifndef LIBFTPRINTF_H
# define LIBFTPRINTF_H
# include <stdarg.h>
# include "../../../includes/libft.h"

typedef struct		s_print
{
	struct s_print	*nxt;
	char			*str;
}					t_print;

typedef struct		s_arg
{
	char			*data;
	char			type;
	char			*mod;
	int				hash;
	int				zero_fill;
	int				left;
	int				pos_sign;
	int				neg_sign;
	int				space;
	int				field;
	int				pre;
}					t_arg;

int					get_arg_data(va_list va, t_arg *arg, char **str);
/*
*********************************CONVERT_F**************************************
*/
int					convert_f(long double nb, t_arg *arg);
int					filter(long double *nb, t_arg *arg);
void				put_signs(t_arg *arg, char **field);
int					flt_size(long double n, t_arg *arg);
long double			get_rounder(int pre, long double nb);

/*
*********************************CONVERT_D**************************************
*/
int					convert_d(long long n, t_arg *arg);
int					dsize(t_arg *arg);
int					put_empty_field(char *field, int size);
long long			check_neg(long long n, t_arg *arg);

/*
*********************************CONVERT_U**************************************
*/
int					convert_u(size_t n, t_arg *arg);
int					usize(t_arg *arg);
int					get_prefix_size(int n, t_arg *arg);
int					get_base(t_arg *arg);

/*
******************************OTHERS CONVERTS***********************************
*/
int					convert_p(uint64_t ptr, t_arg *arg);
int					convert_s(char *s, t_arg *arg);
int					convert_percent(t_arg *arg);
int					convert_c(wchar_t c, t_arg *arg);

/*
********************************STRUCTURES**************************************
*/
t_arg				*init_t_arg(void);
t_print				*init_t_print(void);

/*
**********************************PARSING***************************************
*/

int					get_attibutes(char *str, t_arg *arg, int len);
int					get_modifier(t_arg *arg, char *str);
int					get_type(char *str, t_arg *arg);
int					get_field(char *str, t_arg *arg);
int					get_pre(char *str, t_arg *arg);

#endif
