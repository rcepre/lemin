/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   graph_search_all_utils.c                         .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: krambono <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/03/15 12:28:01 by krambono     #+#   ##    ##    #+#       */
/*   Updated: 2019/03/15 12:28:14 by krambono    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "lem_in.h"

t_dwaylist	*find_dway_all(t_dwaylist *list, int data)
{
	t_dway *tmp;

	while (list)
	{
		tmp = list->dway;
		if (tmp->head->room_i == data)
			return (list);
		list = list->next;
	}
	return (NULL);
}

int			search_room(t_dway *dway, int room_i)
{
	t_way *way;

	if (!dway)
		return (0);
	way = dway->head;
	while (way)
	{
		if (way->room_i == room_i)
			return (1);
		way = way->next;
	}
	return (0);
}
