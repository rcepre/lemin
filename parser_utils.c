/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   parser_utils.c                                   .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rcepre <rcepre@student.42.fr>              +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/03/06 17:49:11 by rcepre       #+#   ##    ##    #+#       */
/*   Updated: 2019/03/18 12:38:15 by rcepre      ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "lem_in.h"

int				check_hashtag(char *line, t_antfarm *antfarm, int count_room)
{
	if (!ft_strcmp(line, "##start"))
	{
		if (antfarm->nb_ant == -1)
			return (0);
		if (antfarm->start != -1)
			return (0);
		antfarm->start = count_room;
	}
	if (!ft_strcmp(line, "##end"))
	{
		if (antfarm->nb_ant == -1)
			return (0);
		if (antfarm->end != -1)
			return (0);
		antfarm->end = count_room;
	}
	return (1);
}

int				check_nb_ant(char *line, int *nb_ant)
{
	int		i;
	long	tmp;

	i = 0;
	while (line[i])
	{
		if (line[i] < '0' || line[i] > '9')
			return (ERROR_FILE);
		i++;
	}
	tmp = ft_strtol(line, NULL, 10);
	if (tmp <= 0 || tmp > FT_INT_MAX)
		return (ERROR_FILE);
	*nb_ant = tmp;
	return (ROOMS);
}

static int		to_roomlist(char *line, t_roomlist **roomlist)
{
	char		*name;
	t_roomlist	*tmp;

	tmp = *roomlist;
	name = ft_strsub(line, 0, ft_search_chr(line, ' '));
	if (name == NULL)
		error_alloc(NULL, NULL, NULL);
	if (name[0] == 'L')
	{
		free(name);
		return (ERROR_FILE);
	}
	while (tmp)
	{
		if (!ft_strcmp(tmp->room->name, name))
		{
			free(name);
			return (ERROR_FILE);
		}
		tmp = tmp->next;
	}
	free(name);
	add_room(line_to_room(line), roomlist);
	return (0);
}

int				check_rooms(char *line, t_roomlist **roomlist)
{
	int		i;
	int		cycle;

	i = 0;
	cycle = 2;
	while (ft_isalnum_custom(line[i]))
		i++;
	if (line[i++] != ' ')
		return (LINKS);
	while (line[i] && cycle)
	{
		while (line[i] >= '0' && line[i] <= '9')
			i++;
		if (line[i] != ' ' && line[i] != '\0')
			return (LINKS);
		cycle--;
		if (!line[i])
			break ;
		i++;
	}
	if (cycle != 0 || line[i])
		return (ERROR_FILE);
	if (to_roomlist(line, roomlist))
		return (ERROR_FILE);
	return (ROOMS);
}

int				check_links(char *line, t_antfarm *antfarm)
{
	static int	fct;
	int			i;

	i = 0;
	while (ft_isalnum_custom(line[i]))
		i++;
	if (line[i] != '-')
		return (!fct ? ERROR_FILE : END_FILE);
	i++;
	while (ft_isalnum_custom(line[i]))
		i++;
	if (line[i] != '\0')
		return (!fct ? ERROR_FILE : END_FILE);
	if (!add_links_to_rooms(line, antfarm))
		return (ERROR_FILE);
	fct = 1;
	antfarm->nb_links++;
	return (LINKS);
}
