/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   antlist.c                                        .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: krambono <krambono@student.42.fr>          +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/02/16 10:43:36 by krambono     #+#   ##    ##    #+#       */
/*   Updated: 2019/03/12 02:10:45 by rcepre      ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "lem_in.h"

int					antlist_size(t_antlist *antlist)
{
	int i;

	i = 0;
	while (antlist->next)
	{
		antlist = antlist->next;
		i++;
	}
	return (i);
}

t_antlist			*create_antlist(t_dway *dway, int ant_id)
{
	t_antlist		*antlist;

	if (!(antlist = (t_antlist*)ft_memalloc(sizeof(t_antlist))))
		return (NULL);
	antlist->ant_id = ant_id;
	antlist->dway = dway_copy(dway);
	antlist->next = NULL;
	return (antlist);
}

void				antlist_add_elem(t_antlist **begin, t_antlist *elem)
{
	t_antlist *tmp;

	if (!*begin)
		*begin = elem;
	else
	{
		tmp = *begin;
		while (tmp->next)
			tmp = tmp->next;
		tmp->next = elem;
	}
}

void				antlist_remove_elem(t_antlist **list, t_antlist *ref)
{
	t_antlist *tmp;
	t_antlist *tmp2;

	if (!*list || !ref)
		return ;
	if (*list == ref)
	{
		tmp = (*list)->next;
		clear_dway(&((*list)->dway));
		free(*list);
		*list = tmp;
		return ;
	}
	tmp = *list;
	tmp2 = tmp->next;
	while (tmp2 && tmp2 != ref)
	{
		tmp = tmp->next;
		tmp2 = tmp->next;
	}
	tmp->next = tmp2->next;
	clear_dway(&(tmp2->dway));
	free(tmp2);
}

void				clear_antlist(t_antlist **begin)
{
	if (!*begin)
		return ;
	clear_dway(&((*begin)->dway));
	if ((*begin)->next)
		clear_antlist(&((*begin)->next));
	free(*begin);
	*begin = NULL;
}
