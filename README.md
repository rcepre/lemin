# Lemin

Ce projet est une introduction à la problématique des algos de pathfinding et aux parcours de graphes.

Il s'agit de trouver la meilleure solution possible pour relier une salle `START` à une salle `END`.<br>
Ces salles peuvent être reliés par un nombre X d'autres salles et doivent être parcourues par un nombre Y de fourmis.<br>
Il ne peut y avoir qu'une fourmi par salle (sauf pour `start` et `end`) et elles ne peuvent se déplacer que d'une salle par tour.<br>
Au premier tour toutes les fourmis sont présentes dans la salle `start`, si la fourmilière transmise est valide le programme s'arrête quand toute les fourmis ont atteint la salle `end`.

Le fichier descriptif de la fourmilière est caractérisé par :
* Son nombre de fourmis
* Ses salles
* Les liaisons entre ces salles

## CAS D'UNE FOURMILIÈRE PARTIELLEMENT/COMPLÈTEMENT INVALIDE

Ce fichier peut être invalide pour différentes raisons :
* Fichier invalide (problème de droit, pas un fichier texte...)
* Il manque le nombre de fourmis/la description des salles/les liaisons
* Il n'y a aucun chemin permettant de relier les salle `start` et `end`
* Les salles `start`/`end` ne sont pas présentes

Dans ces cas, `ERROR` sera imprimé sur la sortie standard.

Si une ligne de la fourmilière n'est pas valide, à savoir :
* Une commande (##something) n'étant pas suivi de `start` ou `end`
* La répétition d'une commande, d'une salle, ou d'une liaison
* Tout autre chose qui n'as rien a faire là

Deux cas de figure sont possibles :

* Si les informations précédents cette ligne fournissent une fourmilière valide, on la traite telle quelle et on ignore le reste du fichier à partir de cette ligne
* Sinon, `ERROR` sera affiché sur la sortie standard.<br><br>

## CAS D'UNE FOURMILIÈRE VALIDE

Dans ce cas, le programme imprimera en premier le contenu du fichier exploité, suivit des déplacements des fourmis (une ligne correspondant à un tour).

Les déplacements des fourmis sont exprimés comme suit (L`numéro de fourmi`-`nom de salle`) :<br>
* 1er tour | `L1-salleA L2-salleB L3-salleC`<br>
* 2e  tour | `L1-salleD L2-salleE L3-salleF`
<br>... etc jusqu'à ce que toute les fourmis soient arrivées à bon port.


## BUILD

Le projet se compile à l'aide de `make` et est exécuté comme suit : `./lem-in < your_map`.



*(thx loiberti)*