/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   t_dway_utils.c                                   .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: krambono <krambono@student.42.fr>          +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/02/11 13:46:30 by krambono     #+#   ##    ##    #+#       */
/*   Updated: 2019/03/12 08:10:21 by rcepre      ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "lem_in.h"

static void	reverse(t_way *way, t_way *prev)
{
	if (!way)
		return ;
	reverse(way->next, way);
	way->next = prev;
}

void		dway_reverse(t_dway **dway)
{
	t_way *tmp;

	tmp = (*dway)->head;
	while (tmp->next)
		tmp = tmp->next;
	reverse((*dway)->head, NULL);
	(*dway)->head = tmp;
}

int			dways_are_differents(t_dway *d1, t_dway *d2)
{
	t_way *w1;
	t_way *w2;

	if ((!d1 && d2) || (d1 && !d2))
		return (0);
	if (!d1 && !d2)
		return (1);
	w1 = d1->head;
	w2 = d2->head;
	while (w1 && w2)
	{
		if (w1->room_i != w2->room_i)
			return (0);
		w1 = w1->next;
		w2 = w2->next;
	}
	return (w1 || w2 ? 0 : 1);
}

int			dways_have_commons(t_dway *d1, t_dway *d2, t_antfarm *antfarm)
{
	t_way	*w1;
	t_way	*w2;

	if ((!d1 && d2) || (d1 && !d2))
		return (0);
	if (!d1 && !d2)
		return (1);
	w1 = d1->head;
	while (w1)
	{
		w2 = d2->head;
		while (w2)
		{
			if (w2->room_i == w1->room_i && w1->room_i != antfarm->start
				&& w1->room_i != antfarm->end && w2->room_i != antfarm->start
												&& w2->room_i != antfarm->end)
				return (1);
			w2 = w2->next;
		}
		w1 = w1->next;
	}
	return (0);
}

t_dway		*dway_copy(t_dway *dway)
{
	t_dway	*cpy;
	t_way	*tmp;

	if (!dway)
		return (NULL);
	if (!(cpy = create_dway()))
		return (NULL);
	cpy->ants = dway->ants;
	tmp = dway->head;
	while (tmp)
	{
		if (!(dway_add_elem_back(&cpy, tmp->room_i)))
			return (NULL);
		tmp = tmp->next;
	}
	return (cpy);
}
