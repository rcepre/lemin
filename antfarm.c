/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   antfarm.c                                        .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rcepre <rcepre@student.le-101.fr>          +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/01/02 09:49:05 by krambono     #+#   ##    ##    #+#       */
/*   Updated: 2019/06/24 04:55:28 by rcepre      ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "lem_in.h"

void	init_antfarm(t_antfarm *antfarm)
{
	antfarm->nb_ant = -1;
	antfarm->rooms = NULL;
	antfarm->nb_room = 0;
	antfarm->nb_links = 0;
	antfarm->start = -1;
	antfarm->end = -1;
}

void	clear_antfarm(t_antfarm *antfarm)
{
	int i;

	if (!antfarm)
		return ;
	i = 0;
	while (i < antfarm->nb_room)
	{
		clear_room(antfarm->rooms[i]);
		i++;
	}
	if (antfarm->rooms)
		free(antfarm->rooms);
}
