# **************************************************************************** #
#                                                           LE - /             #
#                                                               /              #
#    Makefile                                         .::    .:/ .      .::    #
#                                                  +:+:+   +:    +:  +:+:+     #
#    By: krambono <krambono@student.42.fr>          +:+   +:    +:    +:+      #
#                                                  #+#   #+    #+    #+#       #
#    Created: 2018/11/09 13:02:16 by krambono     #+#   ##    ##    #+#        #
#    Updated: 2019/03/18 11:37:00 by rcepre      ###    #+. /#+    ###.fr      #
#                                                          /                   #
#                                                         /                    #
# **************************************************************************** #

SHELL = zsh

# EXECUTABLE
NAME =	lem-in

# SOURCES
SRC = 	antfarm.c \
		display.c \
		get_best_ways.c \
		get_combs.c \
		graph_search.c \
		graph_search_all.c \
		graph_search_all_utils.c \
		lem_in.c \
		parser.c \
		parser_utils.c \
		resolve.c \
		stroke_calcul.c \
		t_antlist.c \
		t_dway.c \
		t_dway_utils.c \
		t_dwaylist.c \
		t_dwaylist_utils.c \
		t_dwaylist_utils2.c \
		t_lines_file.c \
		t_links.c \
		t_queue.c \
		t_roomlist.c \
		t_way.c \
		transform_room.c \
		utilities.c \
		ways_filter.c \

# HEADERS
HEADERS = lem_in.h

# OBJETS
OBJ= $(SRC:.c=.o)

# COMPILATEUR
CC =		gcc
CFLAGS =	-Wall -Wextra -Werror

# LIBS
libft =		./libft/

# REGLES
all: libs $(NAME)

$(NAME): $(OBJ) $(libft)
	@printf "\033[2K\r\033[36m>>Linking...\033[0m\c"
	@$(CC) $(OBJ) -L$(libft) -lft -o $@
	@echo "\t\033[32m[OK]\033[0m"
	@echo "\033[31m...LEM_IN\033[0m"

%.o: %.c $(HEADERS)
	@printf "\033[2K\r\033[36m>>Compiling \033[37m$<\033[36m \033[0m"
	@$(CC) $(CFLAGS) -o $@ -c $<

.PHONY: clean fclean re libs

libs:
	@make -s -C $(libft)

clean:
	@make -s -C $(libft) clean
	@echo "\033[31mCleaning .o\033[0m"
	@rm -f $(OBJ)

fclean:	clean
	@make -s -C $(libft) fclean
	@echo "\033[31mCleaning $(NAME)\033[0m"
	@rm -f $(NAME)

re:		fclean all
