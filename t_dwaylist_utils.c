/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   t_dwaylist_utils.c                               .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: krambono <krambono@student.42.fr>          +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/02/11 14:18:20 by krambono     #+#   ##    ##    #+#       */
/*   Updated: 2019/03/12 08:51:13 by rcepre      ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "lem_in.h"

void			remove_from(t_dwaylist **list, int limit)
{
	int			i;
	t_dwaylist	*tmp;

	if (!*list)
		return ;
	if (limit <= 0)
		clear_dwaylist(list);
	tmp = *list;
	i = 0;
	while (tmp && i < limit - 1)
	{
		i++;
		tmp = tmp->next;
	}
	if (tmp)
		clear_dwaylist(&(tmp->next));
}

int				dwaylist_len(t_dwaylist *list)
{
	int i;

	i = 0;
	while (list)
	{
		i++;
		list = list->next;
	}
	return (i);
}

void			dwaylist_reverse_all(t_dwaylist *list)
{
	while (list)
	{
		dway_reverse(&(list->dway));
		list = list->next;
	}
}

void			dwaylist_remove_dups(t_dwaylist **list)
{
	t_dwaylist *tmp;
	t_dwaylist *tmp2;
	t_dwaylist *tmp3;

	if (!*list || !((*list)->next))
		return ;
	tmp = *list;
	while (tmp)
	{
		tmp2 = tmp->next;
		while (tmp2)
		{
			tmp3 = tmp2->next;
			if (dways_are_differents(tmp->dway, tmp2->dway))
				dwaylist_remove_elem(list, tmp2);
			tmp2 = tmp3;
		}
		tmp = tmp->next;
	}
}

t_dwaylist		*dwaylist_copy(t_dwaylist *list)
{
	t_dwaylist *new_list;

	new_list = NULL;
	while (list)
	{
		dwaylist_add_elem_back(&new_list, dway_copy(list->dway));
		list = list->next;
	}
	return (new_list);
}
