/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   graph_search.c                                   .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: krambono <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/02/13 15:01:27 by krambono     #+#   ##    ##    #+#       */
/*   Updated: 2019/03/15 17:50:41 by krambono    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "lem_in.h"

static void		clean(t_dway **dway, t_dwaylist **list, t_queue **queue)
{
	while (*queue)
		dequeue(queue);
	error_alloc(NULL, dway, list);
}

static t_dway	*find_dway(t_dwaylist *list, int data)
{
	t_dway *tmp;

	while (list)
	{
		tmp = list->dway;
		if (tmp->head->room_i == data)
			return (tmp);
		list = list->next;
	}
	return (NULL);
}

static void		graph_search_2(int room_i, t_dwaylist **list, int end,
																t_queue **queue)
{
	t_dway	*dway;
	t_dway	*tmp;

	tmp = find_dway(*list, end);
	dway = dway_copy(tmp);
	if (tmp && !dway)
		clean(&dway, list, queue);
	if (!dway)
		if (!dway_add_elem_front(&dway, end))
			clean(&dway, list, queue);
	if (!dway_add_elem_front(&dway, room_i))
		clean(&dway, list, queue);
	if (!dwaylist_add_elem_back(list, dway))
		clean(&dway, list, queue);
	if (!enqueue(queue, room_i))
	{
		while (*queue)
			dequeue(queue);
		error_alloc(NULL, NULL, list);
	}
}

static void		graph_search_1(t_antfarm *antfarm, int start, int end,
															t_dwaylist **list)
{
	static int	mark = 1;
	t_queue		*queue;
	t_linklist	*links;

	queue = NULL;
	if (!enqueue(&queue, end))
		error_alloc(NULL, NULL, list);
	antfarm->rooms[end]->mark = mark;
	while (queue)
	{
		end = dequeue(&queue);
		links = antfarm->rooms[end]->linklist;
		while (links && end != start)
		{
			if (antfarm->rooms[links->room_i]->mark != mark ||
				links->room_i == start)
			{
				graph_search_2(links->room_i, list, end, &queue);
				antfarm->rooms[links->room_i]->mark = mark;
			}
			links = links->next;
		}
	}
	clear_dwaylist_if(list, start);
	mark = !mark;
}

void			graph_search(t_antfarm *antfarm, int start, int end,
															t_dwaylist **list)
{
	t_dwaylist	*tmp_list;
	t_dwaylist	*tmp_list2;

	tmp_list = NULL;
	graph_search_1(antfarm, start, end, list);
	graph_search_1(antfarm, end, start, &tmp_list);
	dwaylist_reverse_all(tmp_list);
	while (tmp_list)
	{
		tmp_list2 = tmp_list->next;
		if (!dwaylist_add_elem_back(list, tmp_list->dway))
		{
			clear_dwaylist(&tmp_list);
			error_alloc(NULL, NULL, list);
		}
		free(tmp_list);
		tmp_list = tmp_list2;
	}
	dwaylist_remove_dups(list);
	dwaylist_sort(list);
}
