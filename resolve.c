/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   resolve.c                                        .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rcepre <marvin@le-101.fr>                  +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/02/13 14:03:39 by rcepre       #+#   ##    ##    #+#       */
/*   Updated: 2019/03/18 12:02:00 by krambono    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "lem_in.h"

static void	move_ant(t_antfarm *antfarm, int ant_id, t_antlist *tmp)
{
	int room_dst;
	int room_src;

	room_dst = tmp->dway->head->next->room_i;
	room_src = tmp->dway->head->room_i;
	if (room_dst == antfarm->end)
		antfarm->rooms[room_dst]->ant += 1;
	else
		antfarm->rooms[room_dst]->ant = 1;
	antfarm->rooms[room_src]->ant = 0;
	display_ant_moves(0, ant_id, antfarm->rooms[room_dst]->name, 1);
}

static void	move_ants_ways(t_antfarm *antfarm, t_antlist **list_ant)
{
	t_antlist	*tmp;
	t_antlist	*tmp2;

	tmp = *list_ant;
	while (tmp)
	{
		tmp2 = tmp->next;
		if (tmp->dway->head->next->room_i == antfarm->end)
		{
			move_ant(antfarm, tmp->ant_id, tmp);
			antlist_remove_elem(list_ant, tmp);
		}
		else if (antfarm->rooms[tmp->dway->head->next->room_i]->ant == 0)
		{
			move_ant(antfarm, tmp->ant_id, tmp);
			dway_dequeue(&(tmp->dway));
		}
		tmp = tmp2;
	}
}

void		engage_ants(t_antfarm *antfarm, t_dwaylist **waylist,
										t_antlist **antlist, int *count_ants)
{
	t_dwaylist	*tmp;
	t_dwaylist	*tmp2;
	t_antlist	*tmp_antlist;

	tmp = *waylist;
	while (tmp && *count_ants <= antfarm->nb_ant)
	{
		tmp2 = tmp->next;
		if (antfarm->rooms[tmp->dway->head->room_i]->ant == 0)
		{
			if (tmp->dway->ants == 0)
				dwaylist_remove_elem(waylist, tmp);
			else
			{
				if (!(tmp_antlist = create_antlist(tmp->dway, *count_ants)))
				{
					clear_antlist(antlist);
					error_alloc(NULL, NULL, waylist);
				}
				antlist_add_elem(antlist, tmp_antlist);
				++(*count_ants) && tmp->dway->ants > 0 && tmp->dway->ants--;
			}
		}
		tmp = tmp2;
	}
}

t_dwaylist	*get_baseways(t_antfarm antfarm)
{
	t_dwaylist	*baseways;

	baseways = NULL;
	if (antfarm.nb_room > 50 &&
						(float)antfarm.nb_room / (float)antfarm.nb_links < 0.95)
		graph_search(&antfarm, antfarm.start, antfarm.end, &baseways);
	else
		graph_search_all(&antfarm, &baseways);
	remove_from(&baseways, 100);
	if (!baseways)
	{
		ft_putbuf(NULL, FREE);
		ft_putendl_fd("ERROR", 2);
	}
	return (baseways);
}

int			resolve(t_antfarm *antfarm)
{
	int			ops;
	t_dwaylist	*baseways;
	t_antlist	*antlist;
	int			count_ants;

	count_ants = 1;
	antlist = NULL;
	ops = 0;
	if (!(baseways = get_baseways(*antfarm)))
		return (0);
	ft_putbuf(NULL, !ft_options("m", DECODE) ? DISPLAY : FREE);
	ways_filter(&baseways, antfarm);
	engage_ants(antfarm, &baseways, &antlist, &count_ants);
	while (antfarm->rooms[antfarm->end]->ant != antfarm->nb_ant)
	{
		display_ant_moves(1, 0, NULL, ops);
		move_ants_ways(antfarm, &antlist);
		if (antfarm->rooms[antfarm->start]->ant != antfarm->nb_ant)
			engage_ants(antfarm, &baseways, &antlist, &count_ants);
		ops++;
	}
	if (ops)
		ft_printf("\n");
	clear_dwaylist(&baseways);
	return (ops);
}
