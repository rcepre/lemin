/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   get_best_ways.c                                  .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rcepre <rcepre@student.le-101.fr>          +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/03/01 13:47:16 by krambono     #+#   ##    ##    #+#       */
/*   Updated: 2019/06/24 04:55:25 by rcepre      ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "lem_in.h"

void			add_ants_dway(t_dwaylist *list, int size, int nb_ant)
{
	int			y;

	y = get_strokes_min(list, size, nb_ant);
	list = list->next;
	while (list)
	{
		list->dway->ants = y - list->dway->len + 1;
		list = list->next;
	}
}

void			get_real_way(__uint128_t nb, t_vars *vars)
{
	int			count_min;
	int			i;
	int			tmp;
	t_dwaylist	*list;

	list = NULL;
	count_min = strokes_calcul(nb | ((__uint128_t)1 << 127), vars);
	tmp = count_min;
	i = -1;
	while (++i < vars->size)
	{
		if (!!(nb & ((__uint128_t)1 << (vars->size - 1 - i))) == 1)
		{
			if (count_min > 0)
				if (!(dwaylist_add_elem_back(&list, vars->tab_baseways[i])))
					error_alloc(vars, NULL, &list);
			if (count_min <= 0)
				vars->tab_baseways[i]->ants = 0;
			count_min--;
		}
	}
	if (tmp != 0)
		add_ants_dway(list, tmp, vars->antfarm->nb_ant);
	clear_dwaylist_half(&list);
}

__uint128_t		best_way(__uint128_t nb, t_vars *vars, int ret)
{
	static	__uint128_t	best_way;
	static	int			strokes_min_static = FT_INT_MAX;
	int					strokes_min;

	if (ret)
		return (best_way);
	strokes_min = strokes_calcul(nb, vars);
	if (strokes_min < strokes_min_static)
	{
		best_way = nb;
		strokes_min_static = strokes_min;
	}
	return (best_way);
}
