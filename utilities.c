/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   utilities.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rcepre <rcepre@student.le-101.fr>          +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/01/07 15:55:22 by krambono     #+#   ##    ##    #+#       */
/*   Updated: 2019/06/24 04:55:42 by rcepre      ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "lem_in.h"

void	my_heap(void *adr, int choice)
{
	static unsigned char	index;
	static void				*heap[2];

	if (index == 0)
	{
		heap[0] = NULL;
		heap[1] = NULL;
	}
	if (choice == ADD)
		heap[index++] = adr;
	if (choice == FREE)
	{
		if (heap[1])
			clear_roomlist((t_roomlist**)&heap[1], 1);
		if (heap[0])
			clear_antfarm((t_antfarm*)heap[0]);
	}
	if (choice == ROOMLIST && heap[1])
		clear_roomlist((t_roomlist**)&heap[1], 0);
}

void	error_alloc(t_vars *vars, t_dway **dway, t_dwaylist **dwaylist)
{
	if (dwaylist)
		clear_dwaylist(dwaylist);
	if (vars)
	{
		if (vars->baseways)
			clear_dwaylist(vars->baseways);
		if (vars->comb_tab)
			free(vars->comb_tab);
		if (vars->tab_baseways)
			free(vars->tab_baseways);
	}
	if (dway)
		clear_dway(dway);
	my_heap(NULL, FREE);
	ft_putbuf(NULL, FREE);
	ft_putendl_fd("Allocation failed", 2);
	exit(1);
}

int		check_name_exist(char *name, t_antfarm *antfarm)
{
	int i;

	if (name == NULL)
		error_alloc(NULL, NULL, NULL);
	i = 0;
	while (i < antfarm->nb_room)
	{
		if (!ft_strcmp(antfarm->rooms[i]->name, name))
		{
			free(name);
			return (i);
		}
		i++;
	}
	free(name);
	return (-1);
}

void	ft_putbuf(char *line, int choice)
{
	static t_lines_file *file;

	if (choice == DISPLAY)
	{
		display_lines_file(file);
		ft_putchar('\n');
		clear_lines_file(&file);
	}
	else if (choice == FREE)
		clear_lines_file(&file);
	else if (choice == ADD)
		lines_file_add_elem(&file, line);
}

int		ft_isalnum_custom(int c)
{
	if (ft_isprint(c) && c != '-' && c != ' ' && c != '\n')
		return (1);
	return (0);
}
