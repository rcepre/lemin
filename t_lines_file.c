/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   t_lines_file.c                                   .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rcepre <rcepre@student.le-101.fr>          +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/02/11 08:30:03 by krambono     #+#   ##    ##    #+#       */
/*   Updated: 2019/06/24 04:55:37 by rcepre      ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "lem_in.h"

t_lines_file	*create_lines_file(char *line)
{
	t_lines_file	*file;

	if (!(file = (t_lines_file*)ft_memalloc(sizeof(t_lines_file))))
		error_alloc(NULL, NULL, NULL);
	file->line = line;
	file->next = NULL;
	return (file);
}

void			lines_file_add_elem(t_lines_file **begin, char *line)
{
	t_lines_file *tmp;

	if (!*begin)
		*begin = create_lines_file(line);
	else
	{
		tmp = *begin;
		while (tmp->next)
			tmp = tmp->next;
		tmp->next = create_lines_file(line);
	}
}

void			clear_lines_file(t_lines_file **begin)
{
	if (!*begin)
		return ;
	if ((*begin)->next)
		clear_lines_file(&((*begin)->next));
	free((*begin)->line);
	free(*begin);
	*begin = NULL;
}

void			display_lines_file(t_lines_file *begin)
{
	while (begin)
	{
		ft_putstr(begin->line);
		ft_putchar('\n');
		begin = begin->next;
	}
}
