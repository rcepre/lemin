/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   get_combs.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: krambono <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/03/01 13:47:16 by krambono     #+#   ##    ##    #+#       */
/*   Updated: 2019/03/15 14:07:47 by krambono    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "lem_in.h"

t_dwaylist	*get_comb_list(__uint128_t comb, t_vars *vars)
{
	int			i;
	t_dwaylist	*tmp;
	t_dway		*tmp2;

	tmp = NULL;
	i = 0;
	while (comb >> i)
	{
		if (((comb >> i) & 1) == 1)
		{
			if (!(tmp2 = dway_copy(vars->tab_baseways[vars->size - 1 - i])))
				error_alloc(vars, NULL, NULL);
			if (!(dwaylist_add_elem_front(&tmp, tmp2)))
				error_alloc(vars, &tmp2, &tmp);
		}
		i++;
	}
	return (tmp);
}

static void	recursive(__uint128_t nb, __uint128_t mask, int op, t_vars *vars)
{
	int	j;

	if (!mask)
	{
		best_way(nb, vars, 0);
		return ;
	}
	j = op;
	while (j >= 0)
	{
		if (!!(mask & (__uint128_t)1 << j) == 1)
			recursive(nb | ((__uint128_t)1 << j),
			mask & vars->comb_tab[vars->size - 1 - j], j - 1, vars);
		j--;
	}
}

t_dwaylist	*get_combs(t_vars *vars)
{
	int			i;
	int			j;
	__uint128_t	nb;
	__uint128_t	best;
	t_dwaylist	*new_list;

	i = 0;
	while (i < vars->size)
	{
		nb = 0 | (__uint128_t)1 << (vars->size - 1 - i);
		j = i;
		while (++j < vars->size)
		{
			if (!!((vars->comb_tab[i] << j) &
				(__uint128_t)1 << (vars->size - 1)) == 1)
				recursive(nb | (__uint128_t)1 << (vars->size - 1 - j),
						vars->comb_tab[i] & vars->comb_tab[j],
						vars->size - 1 - j, vars);
		}
		i++;
	}
	best = best_way(nb, vars, 1);
	get_real_way(best, vars);
	new_list = get_comb_list(best, vars);
	return (new_list);
}
