/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   t_dway.c                                         .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: krambono <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/02/11 13:46:30 by krambono     #+#   ##    ##    #+#       */
/*   Updated: 2019/03/15 17:52:36 by krambono    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "lem_in.h"

t_dway		*create_dway(void)
{
	t_dway *dway;

	if (!(dway = (t_dway*)ft_memalloc(sizeof(t_dway))))
		return (NULL);
	dway->head = NULL;
	dway->len = 0;
	dway->ants = -1;
	return (dway);
}

int			dway_add_elem_back(t_dway **dway, int room_i)
{
	t_way	*tmp;
	t_way	*new;

	if (!dway)
		return (1);
	if (!*dway)
		if (!(*dway = create_dway()))
			return (0);
	if (!(new = create_way(room_i)))
		return (0);
	if (!(*dway)->head)
		(*dway)->head = new;
	else
	{
		tmp = (*dway)->head;
		while (tmp->next)
			tmp = tmp->next;
		tmp->next = new;
	}
	(*dway)->len++;
	return (1);
}

int			dway_add_elem_front(t_dway **dway, int room_i)
{
	t_way	*tmp;

	if (!dway)
		return (1);
	if (!*dway)
	{
		if (!(*dway = create_dway()))
			return (0);
		if (!((*dway)->head = create_way(room_i)))
			return (0);
	}
	else
	{
		tmp = (*dway)->head;
		if (!((*dway)->head = create_way(room_i)))
			return (0);
		(*dway)->head->next = tmp;
	}
	(*dway)->len++;
	return (1);
}

void		clear_dway(t_dway **dway)
{
	if (!*dway || !dway)
		return ;
	clear_way(&((*dway)->head));
	free(*dway);
	*dway = NULL;
}

void		dway_dequeue(t_dway **dway)
{
	t_way *way;

	if (!*dway)
		return ;
	way = (*dway)->head;
	if (!way)
		return ;
	(*dway)->head = way->next;
	free(way);
}
