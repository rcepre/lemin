/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   display.c                                        .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rcepre <rcepre@student.le-101.fr>          +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/02/13 11:37:41 by rcepre       #+#   ##    ##    #+#       */
/*   Updated: 2019/06/24 04:55:29 by rcepre      ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "lem_in.h"

void	display_ant_moves(int new_line, int ant_id, char *room_name, int ops)
{
	static int space = 1;

	if (new_line == 1)
	{
		space = 0;
		if (!ft_options("o", DECODE) && ops)
			ft_printf("\n");
		return ;
	}
	if (!ft_options("o", DECODE))
		ft_printf(space ? " L%d-%s" : "L%d-%s", ant_id, room_name);
	space = 1;
}

void	display_infos(t_antfarm antfarm, int ops, char *name)
{
	ft_printf("Map:      %s\n", name);
	ft_printf("Rooms:    %d\n", antfarm.nb_room);
	ft_printf("Links:    %d\n", antfarm.nb_links);
	ft_printf("Ants:     %d\n\n", antfarm.nb_ant);
	ft_printf("Solved in %d operations.\n", ops);
}
