/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ways_filter.c                                    .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: krambono <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/03/01 13:47:16 by krambono     #+#   ##    ##    #+#       */
/*   Updated: 2019/03/15 14:12:17 by krambono    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "lem_in.h"

static void		create_tab_comb(t_vars *vars)
{
	int			i;
	int			j;

	if (!(vars->comb_tab = (__uint128_t*)ft_memalloc(sizeof(__uint128_t) *
																(vars->size))))
		return ;
	i = 0;
	while (i < vars->size)
	{
		j = vars->size - 1;
		while (j >= 0)
		{
			if (!dways_have_commons(vars->tab_baseways[i],
				vars->tab_baseways[vars->size - j - 1], vars->antfarm))
				vars->comb_tab[i] = vars->comb_tab[i] | (__uint128_t)1 << j;
			j--;
		}
		i++;
	}
}

static t_dway	**dwaylist_to_tab(t_dwaylist *list, int size)
{
	t_dway	**tab_list;
	int		i;

	if (!(tab_list = (t_dway**)ft_memalloc(sizeof(t_dway*) * size)))
		return (NULL);
	i = 0;
	while (list)
	{
		tab_list[i++] = list->dway;
		list = list->next;
	}
	return (tab_list);
}

static void		create_vars_data(t_dwaylist **baseways,
											t_antfarm *antfarm, t_vars *vars)
{
	vars->baseways = baseways;
	vars->size = dwaylist_len(*baseways);
	vars->tab_baseways = NULL;
	vars->comb_tab = NULL;
	if (!(vars->tab_baseways = dwaylist_to_tab(*baseways, vars->size)))
		error_alloc(vars, NULL, baseways);
	vars->antfarm = antfarm;
	create_tab_comb(vars);
	if (!vars->comb_tab)
		error_alloc(vars, NULL, NULL);
}

void			ways_filter(t_dwaylist **baseways, t_antfarm *antfarm)
{
	t_dwaylist	*comb_list;
	t_vars		vars;
	t_dway		*tmp;

	create_vars_data(baseways, antfarm, &vars);
	comb_list = get_combs(&vars);
	if (!comb_list)
	{
		if (!(tmp = dway_copy(vars.tab_baseways[0])))
			error_alloc(&vars, &tmp, NULL);
		if (!(dwaylist_add_elem_back(&comb_list, tmp)))
			error_alloc(&vars, &tmp, NULL);
	}
	clear_dwaylist(baseways);
	free(vars.comb_tab);
	free(vars.tab_baseways);
	*baseways = comb_list;
}
