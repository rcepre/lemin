/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   t_way.c                                          .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: krambono <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/02/11 13:46:30 by krambono     #+#   ##    ##    #+#       */
/*   Updated: 2019/03/12 17:36:55 by krambono    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "lem_in.h"

t_way	*create_way(int room_i)
{
	t_way *way;

	if (!(way = (t_way*)ft_memalloc(sizeof(t_way))))
		return (NULL);
	way->room_i = room_i;
	way->next = NULL;
	way->pnodes = 0;
	return (way);
}

void	clear_way(t_way **way)
{
	if (!*way)
		return ;
	if ((*way)->next)
		((*way)->next->pnodes)--;
	clear_way(&((*way)->next));
	if ((*way)->pnodes <= 0)
	{
		free(*way);
		*way = NULL;
	}
}
