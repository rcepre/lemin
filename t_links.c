/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   t_links.c                                        .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rcepre <rcepre@student.le-101.fr>          +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/01/07 15:54:26 by krambono     #+#   ##    ##    #+#       */
/*   Updated: 2019/06/24 04:55:41 by rcepre      ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "lem_in.h"

t_linklist	*create_linklist(int room_i)
{
	t_linklist	*links;

	if (!(links = (t_linklist *)ft_memalloc(sizeof(t_linklist))))
		error_alloc(NULL, NULL, NULL);
	links->room_i = room_i;
	links->next = NULL;
	return (links);
}

void		linklist_add_elem(t_linklist **links, int room_i)
{
	t_linklist *tmp;

	if (!*links)
		*links = create_linklist(room_i);
	else
	{
		tmp = *links;
		if (tmp->room_i == room_i)
			return ;
		while (tmp->next)
		{
			if (tmp->room_i == room_i)
				return ;
			tmp = tmp->next;
		}
		tmp->next = create_linklist(room_i);
	}
}

int			add_links_to_rooms(char *line, t_antfarm *antfarm)
{
	int	first_room;
	int second_room;
	int	dash;

	dash = ft_search_chr(line, '-');
	first_room = check_name_exist(ft_strsub(line, 0, dash), antfarm);
	second_room = check_name_exist(ft_strdup(line + dash + 1), antfarm);
	if (first_room == -1 || second_room == -1 || first_room == second_room ||
		(first_room == antfarm->start && second_room == antfarm->end) ||
		(first_room == antfarm->end && second_room == antfarm->start))
		return (0);
	linklist_add_elem(&(antfarm->rooms[first_room]->linklist), second_room);
	linklist_add_elem(&(antfarm->rooms[second_room]->linklist), first_room);
	return (1);
}

void		clear_linklist(t_linklist **links)
{
	if (!*links)
		return ;
	clear_linklist(&((*links)->next));
	free(*links);
	*links = NULL;
}
