/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   t_dwaylist_utils2.c                              .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rcepre <rcepre@student.le-101.fr>          +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/02/11 14:18:20 by krambono     #+#   ##    ##    #+#       */
/*   Updated: 2019/06/24 04:55:35 by rcepre      ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "lem_in.h"

static	void		clear_if(t_dwaylist **list, t_dwaylist **tmp, int reference)
{
	while ((*list))
	{
		if ((*list)->dway->head->room_i != reference)
		{
			(*tmp)->next = (*list)->next;
			clear_dway(&((*list)->dway));
			free((*list));
			(*list) = (*tmp)->next;
		}
		else
		{
			(*tmp) = (*tmp)->next;
			(*list) = (*tmp)->next;
		}
	}
}

void				clear_dwaylist_if(t_dwaylist **begin, int reference)
{
	t_dwaylist *tmp;
	t_dwaylist *tmp2;

	if (!*begin)
		return ;
	tmp = *begin;
	while (tmp && tmp->dway->head->room_i != reference)
	{
		*begin = tmp->next;
		clear_dway(&(tmp->dway));
		free(tmp);
		tmp = *begin;
	}
	if (!tmp)
		return ;
	tmp2 = tmp->next;
	clear_if(&tmp2, &tmp, reference);
}

void				clear_dwaylist_half(t_dwaylist **list)
{
	if (!*list)
		return ;
	clear_dwaylist_half(&((*list)->next));
	free(*list);
	*list = NULL;
}

static t_dwaylist	*min_dwaylist(t_dwaylist *list)
{
	t_dwaylist *min;

	min = list;
	while (list)
	{
		if (list->dway->len < min->dway->len)
			min = list;
		list = list->next;
	}
	return (min);
}

void				dwaylist_sort(t_dwaylist **list)
{
	t_dwaylist *sorted_list;
	t_dwaylist *min;

	if (!*list || !(*list)->next)
		return ;
	sorted_list = NULL;
	while (*list)
	{
		min = min_dwaylist(*list);
		dwaylist_add_elem_back(&sorted_list, dway_copy(min->dway));
		dwaylist_remove_elem(list, min);
	}
	*list = sorted_list;
}
